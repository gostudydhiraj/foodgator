<?php

namespace App\Http\Controllers;

use Input;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
 public function anyIndex(){

  return view('index');
 }



  public function anyRegister(Request $request){

        if (Input::get()) {
            $fname = Input::get('fname');
            
            $lname = Input::get('lname');

            $name = $fname.' '.$lname;
            
            $email = Input::get('email');

            $number = Input::get('phone');

            $pass = Input::get('user_pass');


            $password = bcrypt($pass);

            
            $required_points = [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|unique:user_profile,email,NULL,id,active,1',
            'user_pass' => 'required|confirmed',
            'phone' => 'required|unique:user_profile,phone,NULL,id,active,1'
            ];

            $validator = Validator::make($request->all(),$required_points);

            if ($validator->fails()) {
                return redirect('user/register')
                        ->withErrors($validator)
                        ->withInput();
            }

            $user_id =  \App\UserProfile::insertGetId([
                                'name' => $name,
                                'email' => $email,
                                'phone' => $number,
                                'password' => $password,
                                'active' => 1,
                                'created_utc' => \Carbon\Carbon::now()
                              ]);
}
  	return view('register');
  

  }
      public function anyLogin(Request $request)
    {

        if (Input::get()) {
            $password = Input::get('pass');

            $email = trim(Input::get('email'));

            $verify = ["password"=>$password, "email"=>$email, "active" => 1];

            if (Auth::attempt($verify)) { //if is successful then it returns true


            echo "hi";
        } else {

                $message = "User doesn't exist in our system";


            }
        }

        return view('login');
    }


}
