<?php

namespace App\Http\Controllers;

use Input;
use Auth;
use DB;
use Illuminate\Http\Request;
use Validator;

class ApiController extends Controller
{
 public function anyRegister(Request $request){
/*$test1=["Kaustabh","Kinshuk","Bhopoji"];
$test = [];
$test["name"]="Kaustabh";
$test["age"] = 22;
$test["simple"] = $test1;
$test2 = "key => value";
$test["associative"] = ["first"=>"Kaustabh","last"=>"Lahiri","age"=>22];


return $test;*/
 $status = [];
  $json = file_get_contents('php://input');

   
  $obj=json_decode($json);

   $a = $obj->first_name;
   $b = $obj->last_name;
   $name = $a.' '.$b;
   $email = $obj->email;
   $number = $obj->phone;
   $pass = $obj->password;
   $password = bcrypt($pass);

   $required_points = [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|unique:user_profile,email,NULL,id,active,1',
            'user_pass' => 'required|confirmed',
            'phone' => 'required|unique:user_profile,phone,NULL,id,active,1'
            ];

            $validator = Validator::make($request->all(),$required_points);

            if ($validator->fails()) {
                return redirect('user/register')
                        ->withErrors($validator)
                        ->withInput();
            }

  
  $user_id =  \App\UserProfile::insertGetId([
                                'name' => $name,
                                'email' => $email,
                                'phone' => $number,
                                'password' => $password,
                                'active' => 1,
                                'created_utc' => \Carbon\Carbon::now()
                              ]);

  if(!empty($user_id)){
  	$status["status"] = 1;
  	$status["message"] = "User registeration complete";
  } else{

  	$status["status"] = 0;
  	$status["message"] = "User registeration failed";
  }

 return $status;
 }

}
